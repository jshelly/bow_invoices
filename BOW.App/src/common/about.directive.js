﻿(function () {
    'use strict';

    angular.module('app.directives.about', ['app'])

        .directive('appAbout', appAbout);

    appAbout.$inject = ['APP_NAME', 'APP_VERSION', 'APP_AUTHOR'];

    function appAbout(APP_NAME, APP_VERSION, APP_AUTHOR) {
        console.log('App.About running...')
        return function (scope, elm, attrs) {
            elm.text(APP_NAME + ' v' + APP_VERSION + ' by ' + APP_AUTHOR);
        };
    }
})();