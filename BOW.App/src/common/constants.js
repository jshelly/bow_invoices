﻿(function () {
    'use strict';

    angular.module('app')

        .constant('APP_AUTHOR', 'Jim Shelly')
        .constant('APP_NAME', 'AngularJS Sample App')
        .constant('APP_VERSION', '0.0.1');
})();