﻿(function () {
    'use strict';

    console.log("Invoice Controller Running...")
    angular.module('app.invoice', ['app.invoiceService'])

        .controller('InvoiceController', InvoiceController);

    InvoiceController.$inject = ['$scope', '$log', 'invoiceService'];

    function InvoiceController($scope, $log, invoiceService) {
        var vm = this;

        vm.invoices = [];

        retrieve();

        function retrieve() {
            return getInvoices().then(function () {
                $log.info('Retrieved Invoices');
            });
        };

        function getInvoices() {
            return invoiceService.getInvoices()
                .then(function (data) {
                    vm.invoices = data;
                    return vm.invoices;
                });
        };
    }
})();