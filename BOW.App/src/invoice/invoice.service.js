﻿(function () {
    'use strict';

    angular.module('app.invoiceService', [])

        .factory('invoiceService', invoiceService);

    invoiceService.$inject = ['$http', '$log', '$q'];

    function invoiceService($http, $log, $q) {
        return {
            getInvoices: getInvoices
        };

        function getInvoices() {
            return $http.get('/src/assets/data/invoices.json')
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    var newMessage = 'XHR Failed for getInvoices.';
                    $log.error(newMessage);
                    return $q.reject(error);
                });          
        }
    }
})();