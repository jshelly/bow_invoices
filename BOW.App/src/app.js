﻿(function () {
    'use strict';
    console.log("App.js running...");
    angular.module('app', [
        'ui.router',
        'app.home',
        'app.invoice',
        'app.nav.header',
        'app.nav.menu',
        'app.nav.footer',
        'app.directives.datepicker',
        'app.directives.about',
        'app.filters'
    ]);
})();