﻿(function () {
    'use strict';

    console.log("app.routes Running...");
    angular.module('app')

        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {        
        $urlRouterProvider.when('', '/invoices/list');        
        $urlRouterProvider.otherwise('');
        $stateProvider
            .state('root', {
                abstract: true,
                url: '/',
                data: {
                    title: 'Home',
                },
                views: {
                    'header': {
                        templateUrl: 'src/navigation/header.html',
                        controller: 'HeaderController',
                        controllerAs: 'HDC'
                    },
                    'menu': {
                        templateUrl: 'src/navigation/menu.html',
                        controller: 'MenuController',
                        controllerAs: 'MC'
                    },
                    'content': {
                        template: 'Choose option from menu...'
                    },
                    'footer': {
                        templateUrl: 'src/navigation/footer.html',
                        controller: 'FooterController',
                        controllerAs: 'FC'
                    }
                }
            })
            .state('root.invoices', {
                abstract: true,
                url: 'invoices',
                data: {
                    title: 'Invoices',
                }
            })
            .state('root.invoices.list', {
                url: '/list',
                data: {
                    title: 'Invoice List',
                },
                views: {
                    'content@': {
                        templateUrl: 'src/invoice/invoice.html',
                        controller: 'InvoiceController',
                        controllerAs: 'IC'
                    }
                }
            })
            .state('root.invoice.new', {
                url: '/new',
                data: {
                    title: 'New Invoice',
                },
                views: {
                    'content@': {
                        templateUrl: 'src/invoice/invoicenew.html',
                        controller: 'InvoiceNewController',
                        controllerAs: 'INC'
                    }
                }
            })
        console.log('State', $stateProvider);
    }

    
})();