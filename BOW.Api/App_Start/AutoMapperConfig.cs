﻿using AutoMapper;
using BOW.Lib.Customers;
using BOW.Lib.InventoryStuff;
using BOW.Lib.Invoices;
using BOW.Lib.Services;
using System;
using System.Web.Http;

namespace BOW.Api
{
    public class AutoMapperConfig
    {
        protected internal static IMapper Mapper;

        static Func<Type, object> GetResolver(HttpConfiguration config) => type => config.DependencyResolver.GetService(type);

        public static IMapper Configure(HttpConfiguration config)
        {
            Action<IMapperConfigurationExpression> mapperConfigurationExp = cfg =>
            {
                cfg.ConstructServicesUsing(GetResolver(config));

                // TODO: Create mappings here
                cfg.CreateMap<Invoice, InvoiceDTO>();
                cfg.CreateMap<Address, AddressDTO>();
                cfg.CreateMap<Customer, CustomerDTO>();
                cfg.CreateMap<Inventory, InventoryDTO>();
                cfg.CreateMap<Material, MaterialDTO>();
                cfg.CreateMap<Service, ServiceDTO>();
                cfg.CreateMap<InvoiceDTO, Invoice>();
                cfg.CreateMap<AddressDTO, Address>();
                cfg.CreateMap<CustomerDTO, Customer>();
                cfg.CreateMap<InventoryDTO, Inventory>();
                cfg.CreateMap<MaterialDTO, Material>();
                cfg.CreateMap<ServiceDTO, Service>();

                // For more information see https://github.com/drwatson1/AspNet-WebApi/wiki#automapper
            };

            var mapperConfiguration = new MapperConfiguration(mapperConfigurationExp);
            Mapper = mapperConfiguration.CreateMapper();

            return Mapper;
        }
    }
}