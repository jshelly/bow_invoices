﻿using BOW.Lib.utils;
using Serilog;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BOW.Api.Models;
using System.Collections.Generic;
using System.Diagnostics;
using BOW.Lib.Invoices;
using System.Configuration;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Azure.Storage.Blobs;
using System.Linq;
using AutoMapper;

namespace BOW.Api.Controllers
{
    public class UploadController : ApiController
    {
        IParser _parser;
        ILogger _logger;
        IInvoiceService _invoiceService;
        private IMapper _mapper;

        public UploadController(IParser parser, ILogger logger, IMapper mapper, IInvoiceService invoiceService)
        {
            Serilog.Debugging.SelfLog.Enable(msg => Debug.WriteLine(msg));

            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _invoiceService = invoiceService;
            _parser = parser;
            _mapper = mapper;
        }
        // POST: api/Upload
        public async Task<HttpResponseMessage> Post()
        {
            _logger.Information("Start PDF Import for...");
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());

            IList<HttpContent> files = provider.Files;
            HttpContent file1 = files[0];
            string fileName = files[0].Headers.ContentDisposition.FileName.Trim('\"');
            Stream input = await file1.ReadAsStreamAsync();

            try
            {
                _logger.Information("Importing file {0}", fileName);
                //_logger.Information("Upload the file {FileName} to the cloud.", fileName);
                //string connectionString = ConfigurationManager.AppSettings["BOWStorageConnectionString"];
                //string containerName = ConfigurationManager.AppSettings["my-container"];

                //BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);
                //BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(containerName);
                //containerClient.GetBlobClient(fileName).Upload(input, overwrite: true);
            }
            catch(Exception ex)
            {
                _logger.Error(ex, "There was an error uploading file {FileName}", fileName);
            }            
            _parser = new PdfParser(_logger, _invoiceService);
            var _ms = new MemoryStream();

            input.Position = 0;
            input.CopyTo(_ms);
            input.Close();
            _logger.Information("Start PDF Parsing...");
            try
            {
                _parser.Process(_ms);
                _ms.Close();
                _logger.Information("End PDF Parsing...");
                _logger.Information("Get latest invoices");
                IEnumerable<InvoiceDTO> invoices = _invoiceService.Invoices()
                .Select(_mapper.Map<InvoiceDTO>)
                .OrderByDescending(o => o.EstimatedSrvDate)
                .ThenBy(o => o.EstimatedArrTime)
                .ToList<InvoiceDTO>();
                return Request.CreateResponse(HttpStatusCode.OK, invoices);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error Occurred Parsing file {File}", files[0].Headers.ContentDisposition.FileName);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
