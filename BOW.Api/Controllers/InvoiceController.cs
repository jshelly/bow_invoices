﻿using AutoMapper;
using BOW.Lib.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BOW.Api.Controllers
{
    public class InvoiceController : ApiController
    {
        private IInvoiceService _invoiceService;
        private IMapper _mapper;

        public InvoiceController(IMapper mapper, IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
            _mapper = mapper;
        }
        // GET: api/Invoice
        public IEnumerable<InvoiceDTO> Get()
        {
            return _invoiceService.Invoices()
                .Select(_mapper.Map<InvoiceDTO>)
                .OrderByDescending(o => o.EstimatedSrvDate)
                .ThenBy(o => o.EstimatedArrTime)
                .ToList<InvoiceDTO>();
        }

        // GET: api/Invoice/5
        public IEnumerable<InvoiceDTO> Get(string date)
        {
            var result =  _invoiceService.Invoices()
                .Where(x => x.EstimatedSrvDate.Contains(date))
                .Select(_mapper.Map<InvoiceDTO>)
                .ToList<InvoiceDTO>();

            return result;
        }

        // POST: api/Invoice
        public void Post([FromBody]InvoiceDTO invoice)
        {
            Invoice inv = _mapper.Map<Invoice>(invoice);
            _invoiceService.Save(inv);

        }

        // PUT: api/Invoice/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Invoice/5
        public void Delete(Guid id)
        {
            var result = _invoiceService.Delete(id);
            //"03a28837-8602-4148-b213-68aa89f7e268"
        }
    }
}
