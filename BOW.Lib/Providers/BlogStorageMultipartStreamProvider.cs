﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Microsoft.Azure;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.File;

namespace BOW.Lib.Providers
{
    public class FileShareMultipartStreamProvider : MultipartStreamProvider
    {
        public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
        {
            Stream stream = null;
            ContentDispositionHeaderValue contentDisposition = headers.ContentDisposition;
            if (contentDisposition != null)
            {

                if (!String.IsNullOrWhiteSpace(contentDisposition.FileName))
                {
                    string fileName = contentDisposition.FileName.Replace(".pdf", "").Replace("\"","");
                    string connectionString = ConfigurationManager.AppSettings["BOWStorageConnectionString"];
                    //string containerName = ConfigurationManager.AppSettings["my-container"];
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
                    CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                    CloudFileShare share = fileClient.GetShareReference("pdffiles");

                    if(share.Exists())
                    {
                        //if the upload directory doesn't exist then create it.
                        CloudFileDirectory rootDir = share.GetRootDirectoryReference();
                        CloudFileDirectory uploadDir = rootDir.GetDirectoryReference("uploads");
                        uploadDir.CreateIfNotExists();

                        //now create the file upload directory
                        CloudFileDirectory fileDir = uploadDir.GetDirectoryReference(fileName);
                        fileDir.CreateIfNotExists();
                        //fileDir.GetFileReference("fileName").UploadFromStream(contentDisposition);
                    }                   
                }
            }
            return stream;
        }
    }
}
