﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Defaults2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice");
            DropPrimaryKey("dbo.Inventories");
            DropPrimaryKey("dbo.Invoice");
            DropPrimaryKey("dbo.Materials");
            DropPrimaryKey("dbo.Services");
            AlterColumn("dbo.Inventories", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Invoice", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Materials", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Services", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Inventories", "Id");
            AddPrimaryKey("dbo.Invoice", "Id");
            AddPrimaryKey("dbo.Materials", "Id");
            AddPrimaryKey("dbo.Services", "Id");
            AddForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice");
            DropPrimaryKey("dbo.Services");
            DropPrimaryKey("dbo.Materials");
            DropPrimaryKey("dbo.Invoice");
            DropPrimaryKey("dbo.Inventories");
            AlterColumn("dbo.Services", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Materials", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Invoice", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Inventories", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.Services", "Id");
            AddPrimaryKey("dbo.Materials", "Id");
            AddPrimaryKey("dbo.Invoice", "Id");
            AddPrimaryKey("dbo.Inventories", "Id");
            AddForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
        }
    }
}
