﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Defaults : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Addresses");
            AlterColumn("dbo.Addresses", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Addresses", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Addresses");
            AlterColumn("dbo.Addresses", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.Addresses", "Id");
        }
    }
}
