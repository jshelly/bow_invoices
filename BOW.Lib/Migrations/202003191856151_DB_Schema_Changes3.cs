﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DB_Schema_Changes3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Addresses", "Type", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Addresses", "Type", c => c.Int(nullable: false));
        }
    }
}
