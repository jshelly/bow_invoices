﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix_Misspelling : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Services", "Invoice_Id", "dbo.Invoice");
            DropIndex("dbo.Services", new[] { "Invoice_Id" });
            RenameColumn(table: "dbo.Services", name: "Invoice_Id", newName: "InvoiceId");
            AlterColumn("dbo.Services", "InvoiceId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Services", "InvoiceId");
            AddForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            DropColumn("dbo.Services", "InvoideId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Services", "InvoideId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice");
            DropIndex("dbo.Services", new[] { "InvoiceId" });
            AlterColumn("dbo.Services", "InvoiceId", c => c.Guid());
            RenameColumn(table: "dbo.Services", name: "InvoiceId", newName: "Invoice_Id");
            CreateIndex("dbo.Services", "Invoice_Id");
            AddForeignKey("dbo.Services", "Invoice_Id", "dbo.Invoice", "Id");
        }
    }
}
