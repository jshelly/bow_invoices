﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_Setup1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Invoices", newName: "Invoice");
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Street1 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        ZipCode = c.String(),
                        Type = c.Int(nullable: false),
                        Description = c.String(),
                        Floors = c.Double(nullable: false),
                        Walking = c.Int(nullable: false),
                        PropertyType = c.String(),
                        CustomerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        PhoneNo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Description = c.String(),
                        Quantity = c.Int(nullable: false),
                        Rate = c.Double(nullable: false),
                        Amount = c.Double(nullable: false),
                        InvoideId = c.Guid(nullable: false),
                        Invoice_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoice", t => t.Invoice_Id)
                .Index(t => t.Invoice_Id);
            
            AddColumn("dbo.Inventories", "InvoiceId", c => c.Guid(nullable: false));
            AddColumn("dbo.Invoice", "Customer_Id", c => c.Guid());
            AlterColumn("dbo.Invoice", "EstimatedSrvDate", c => c.String(nullable: false));
            AlterColumn("dbo.Invoice", "EstimatedArrTime", c => c.String(nullable: false));
            CreateIndex("dbo.Inventories", "InvoiceId");
            CreateIndex("dbo.Invoice", "Customer_Id");
            AddForeignKey("dbo.Invoice", "Customer_Id", "dbo.Customers", "Id");
            AddForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            DropColumn("dbo.Inventories", "InventoryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Inventories", "InventoryId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Services", "Invoice_Id", "dbo.Invoice");
            DropForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Invoice", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.Addresses", "CustomerId", "dbo.Customers");
            DropIndex("dbo.Services", new[] { "Invoice_Id" });
            DropIndex("dbo.Invoice", new[] { "Customer_Id" });
            DropIndex("dbo.Inventories", new[] { "InvoiceId" });
            DropIndex("dbo.Addresses", new[] { "CustomerId" });
            AlterColumn("dbo.Invoice", "EstimatedArrTime", c => c.String());
            AlterColumn("dbo.Invoice", "EstimatedSrvDate", c => c.String());
            DropColumn("dbo.Invoice", "Customer_Id");
            DropColumn("dbo.Inventories", "InvoiceId");
            DropTable("dbo.Services");
            DropTable("dbo.Customers");
            DropTable("dbo.Addresses");
            RenameTable(name: "dbo.Invoice", newName: "Invoices");
        }
    }
}
