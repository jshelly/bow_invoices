﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_invoice_name : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Invoice", newName: "Invoices");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Invoices", newName: "Invoice");
        }
    }
}
