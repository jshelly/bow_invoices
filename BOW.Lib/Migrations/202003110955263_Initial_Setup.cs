﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_Setup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Inventories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Description = c.String(),
                        Count = c.Int(nullable: false),
                        InventoryId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        InvoiceNo = c.Int(nullable: false),
                        QuoteNo = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        NotToExceed = c.Boolean(nullable: false),
                        Binding = c.Boolean(nullable: false),
                        EstimatedSrvDate = c.String(),
                        EstimatedArrTime = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Materials",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Description = c.String(),
                        Count = c.Int(nullable: false),
                        InvoiceId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId, cascadeDelete: true)
                .Index(t => t.InvoiceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoices");
            DropIndex("dbo.Materials", new[] { "InvoiceId" });
            DropTable("dbo.Materials");
            DropTable("dbo.Invoices");
            DropTable("dbo.Inventories");
        }
    }
}
