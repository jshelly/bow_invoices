﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_sortorder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "SortOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Addresses", "SortOrder");
        }
    }
}
