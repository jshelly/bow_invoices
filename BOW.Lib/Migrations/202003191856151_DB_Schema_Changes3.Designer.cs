﻿// <auto-generated />
namespace BOW.Lib.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class DB_Schema_Changes3 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DB_Schema_Changes3));
        
        string IMigrationMetadata.Id
        {
            get { return "202003191856151_DB_Schema_Changes3"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
