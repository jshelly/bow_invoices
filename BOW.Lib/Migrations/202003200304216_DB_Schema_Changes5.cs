﻿namespace BOW.Lib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DB_Schema_Changes5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Addresses", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Invoice", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice");
            DropPrimaryKey("dbo.Customers");
            DropPrimaryKey("dbo.Inventories");
            DropPrimaryKey("dbo.Invoice");
            DropPrimaryKey("dbo.Materials");
            DropPrimaryKey("dbo.Services");
            AlterColumn("dbo.Customers", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Inventories", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Invoice", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Materials", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Services", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.Customers", "Id");
            AddPrimaryKey("dbo.Inventories", "Id");
            AddPrimaryKey("dbo.Invoice", "Id");
            AddPrimaryKey("dbo.Materials", "Id");
            AddPrimaryKey("dbo.Services", "Id");
            AddForeignKey("dbo.Addresses", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Invoice", "Customer_Id", "dbo.Customers", "Id");
            AddForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Invoice", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.Addresses", "CustomerId", "dbo.Customers");
            DropPrimaryKey("dbo.Services");
            DropPrimaryKey("dbo.Materials");
            DropPrimaryKey("dbo.Invoice");
            DropPrimaryKey("dbo.Inventories");
            DropPrimaryKey("dbo.Customers");
            AlterColumn("dbo.Services", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Materials", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Invoice", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Inventories", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Customers", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Services", "Id");
            AddPrimaryKey("dbo.Materials", "Id");
            AddPrimaryKey("dbo.Invoice", "Id");
            AddPrimaryKey("dbo.Inventories", "Id");
            AddPrimaryKey("dbo.Customers", "Id");
            AddForeignKey("dbo.Services", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Materials", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Inventories", "InvoiceId", "dbo.Invoice", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Invoice", "Customer_Id", "dbo.Customers", "Id");
            AddForeignKey("dbo.Addresses", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
        }
    }
}
