﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using BOW.Lib.Customers;
using BOW.Lib.InventoryStuff;
using BOW.Lib.Invoices;
using BOW.Lib.Services;
using ExcelDataReader;

namespace BOW.Lib.utils
{
    public class XlsParser : IParser
    {
        private Tuple<int, int> InventoryPoints;
        private Tuple<int, int> MaterialsPoints;
        private Tuple<int, int> ServicePoints;
        private Tuple<int, int> HoursWorkedPoints;
        private Tuple<int, int> TotalDuePoints;
        private Tuple<int, int> PaymentMethodPoints;
        private Tuple<int, int> QtyPoints;
        private Tuple<int, int> RatePoints;
        private Tuple<int, int> AmountPoints;
        private Tuple<int, int> ShipperPoints;
        private Tuple<int, int> DestinationPoints;
        private Tuple<int, int> OriginPoints;
        private Tuple<int, int> InvoicePoints;
        private Tuple<int, int> Stop1Points;
        private Tuple<int, int> Stop2Points;
        private Tuple<int, int> AddendumPoints;
        private Tuple<int, int> mileagePoints;
        private Tuple<int, int> volumePoints;
        private Tuple<int, int> manHoursPoints;
        private Tuple<int, int> secondPage;
        private Tuple<int, int> firstPage;
        private bool isInvoice;
        private Invoice invoice;
        private int colCount;
        private DataSet ds;

        public XlsParser() { }
        public XlsParser(Invoice invoice)
        {
            this.invoice = invoice;
        }

        public bool Process(MemoryStream stream)
        {
            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                ds = reader.AsDataSet();
                colCount = ds.Tables[0].Columns.Count;

                this.firstPage = GetStartPoint(ds, "Page 1", false);
                this.isInvoice = this.firstPage != null;
                this.secondPage = GetStartPoint(ds, "Page 2 of2");

                if (isInvoice)
                {
                    this.InventoryPoints = GetStartPoint(ds, "Inventory");
                    this.MaterialsPoints = GetStartPoint(ds, "Materials");
                    this.ServicePoints = GetStartPoint(ds, "Service");
                    this.HoursWorkedPoints = GetStartPoint(ds, "Hours Worked");
                    this.TotalDuePoints = GetStartPoint(ds, "TOTAL DUE:");
                    this.PaymentMethodPoints = GetStartPoint(ds, "Payment Method");
                    this.QtyPoints = GetStartPoint(ds, "Qty");
                    this.RatePoints = GetStartPoint(ds, "Rate");
                    this.AmountPoints = GetStartPoint(ds, "Amount");
                    this.ShipperPoints = GetStartPoint(ds, "Shipper");
                    this.DestinationPoints = GetStartPoint(ds, "Destination");
                    this.OriginPoints = GetStartPoint(ds, "Origin");
                    this.InvoicePoints = GetStartPoint(ds, "Invoice:", false);
                    this.Stop1Points = GetStartPoint(ds, "Extra Stop 1:");
                    this.Stop2Points = GetStartPoint(ds, "Extra Stop 2:");
                }                
            }

            if (this.isInvoice)
            {
                parseInvoiceInfo(ds.Tables[0], invoice);
                parseInventory(ds.Tables[0], invoice);
                parseMaterials(ds.Tables[0], invoice);
                parseServices(ds.Tables[0], invoice);
                parseShipper(ds.Tables[0], invoice);
                parseOrigin(ds.Tables[0], invoice);
                parseDestination(ds.Tables[0], invoice);
            }            

            return this.isInvoice;
        }

        private void parseInvoiceInfo(DataTable dt, Invoice invoice)
        {
            IDictionary<string, string> invoiceDict = new Dictionary<string, string>();

            List<string> invInfo = new List<string>();
            var InvoiceInfo = "";

            if (!String.IsNullOrEmpty(dt.Rows[this.InvoicePoints.Item1][this.InvoicePoints.Item2].ToString()))
            {
                InvoiceInfo = dt.Rows[this.InvoicePoints.Item1][this.InvoicePoints.Item2].ToString();
            }

            var list = InvoiceInfo.Split('\n');
            string[] delimiter = { ": " };
            for (int i = 0; i < list.Length; i++)
            {
                string key = list[i].Split(delimiter, StringSplitOptions.None)[0];
                string value = list[i].Split(delimiter, StringSplitOptions.None)[1];
                switch (key)
                {
                    case "Invoice":
                        invoice.InvoiceNo = Convert.ToInt32(value);
                        break;
                    case "Quote":
                        invoice.QuoteNo = Convert.ToInt32(value);
                        break;
                    case "Date":
                        invoice.EstimatedSrvDate = value;
                        break;
                    case "Arrival":
                        invoice.EstimatedArrTime = value;
                        break;
                }
            }
        }

        private void parseInventory(DataTable dt, Invoice invoice)
        {
            string inv = "";

            for (var i = InventoryPoints.Item1 + 2; i < MaterialsPoints.Item1; i++)
            {
                for (var j = 0; j < colCount; j++)
                {
                    if (!String.IsNullOrEmpty(dt.Rows[i][j].ToString()))
                    {
                        string item = "";
                        int result;
                        Int32.TryParse(dt.Rows[i][j].ToString(), out result);
                        inv += dt.Rows[i][j].ToString() + "|";
                        string value = dt.Rows[i][j].ToString();

                        if (result == 0)
                        {
                            item = dt.Rows[i][j].ToString();
                            inv += "\n";
                        }
                    }
                }
            }

            string[] lines = inv.Split('\n');
            List<Tuple<int, int, string>> InvItem = new List<Tuple<int, int, string>>();

            for (var i = 0; i < lines.Length; i++)
            {
                var items = lines[i].Split('|');
                if (!String.IsNullOrEmpty(items[0]))
                    InvItem.Add(new Tuple<int, int, string>(0, Convert.ToInt32(items[0]), items[1]));
            }

            List<Inventory> inventory = new List<Inventory>();
            foreach (var item in InvItem)
            {
                Inventory invItem = new Inventory();
                invItem.Count = item.Item2;
                invItem.Description = item.Item3;
                invItem.Invoice = invoice;
                inventory.Add(invItem);
            }
            invoice.Inventories = inventory;
        }

        private void parseMaterials(DataTable dt, Invoice invoice)
        {
            string mats = "";

            for (var i = this.MaterialsPoints.Item1; i < this.HoursWorkedPoints.Item1; i++)
            {
                for (var j = 0; j < colCount; j++)
                {
                    if (!String.IsNullOrEmpty(dt.Rows[i][j].ToString()))
                    {
                        string item = "";
                        int result;
                        Int32.TryParse(dt.Rows[i][j].ToString(), out result);
                        mats += dt.Rows[i][j].ToString() + "|";
                        string value = dt.Rows[i][j].ToString();

                        if (result == 0)
                        {
                            item = dt.Rows[i][j].ToString();
                            mats += "\n";
                        }
                    }
                }
            }
            string[] lines = mats.Split('\n');
            List<Tuple<int, int, string>> MatItems = new List<Tuple<int, int, string>>();

            for (var i = 1; i < lines.Length; i++)
            {
                var items = lines[i].Split('|');
                if (!String.IsNullOrEmpty(items[0]))
                    MatItems.Add(new Tuple<int, int, string>(0, Convert.ToInt32(items[0]), items[1]));
            }

            List<Material> materials = new List<Material>();
            foreach (var item in MatItems)
            {
                Material matItem = new Material();
                matItem.Count = item.Item2;
                matItem.Description = item.Item3;
                materials.Add(matItem);
            }
            invoice.Materials = materials;
        }

        private void parseServices(DataTable dt, Invoice invoice)
        {
            int srvCol = this.ServicePoints.Item2;
            int qtyCol = this.QtyPoints.Item2;
            int rateCol = this.RatePoints.Item2;
            int amtCol = this.AmountPoints.Item2;
            int srvRow = this.ServicePoints.Item1;

            List<Tuple<string, int, double, double>> servicesList = new List<Tuple<string, int, double, double>>();

            for (int i = srvRow + 1; i < this.TotalDuePoints.Item1; i++)
            {
                Tuple<string, int, double, double> srvTuple; ;
                string service = "";
                string qty = "";
                string rate = "";
                string amount = "";

                if (!String.IsNullOrEmpty(dt.Rows[i][srvCol].ToString()))
                    service = dt.Rows[i][srvCol].ToString();
                if (!String.IsNullOrEmpty(dt.Rows[i][qtyCol].ToString()))
                    qty = dt.Rows[i][qtyCol].ToString();
                if (!String.IsNullOrEmpty(dt.Rows[i][rateCol].ToString()))
                    rate = dt.Rows[i][rateCol].ToString();
                if (!String.IsNullOrEmpty(dt.Rows[i][amtCol].ToString()))
                    amount = dt.Rows[i][amtCol].ToString();

                int tryQty;
                Int32.TryParse(qty, out tryQty);
                double tryRate;
                double.TryParse(rate.Replace("$", "").Replace(" ", ""), out tryRate);
                double tryAmount;
                double.TryParse(amount.Replace("$", "").Replace(" ", ""), out tryAmount);

                if (!String.IsNullOrEmpty(service))
                {
                    srvTuple = new Tuple<string, int, double, double>(service, tryQty, tryRate, tryAmount);
                    servicesList.Add(srvTuple);
                }

                List<Service> Services = new List<Service>();
                foreach (var item in servicesList)
                {
                    Service srvItem = new Service();
                    srvItem.Description = item.Item1;
                    srvItem.Quantity = item.Item2;                    
                    srvItem.Rate = item.Item3;
                    srvItem.Amount = item.Item4;
                    Services.Add(srvItem);
                }
                invoice.Services = Services;
            }
        }

        private void parseShipper(DataTable dt, Invoice invoice)
        {
            var shipper = "";
            for (var j = 1; j < this.OriginPoints.Item2; j++)
            {
                if (!String.IsNullOrEmpty(dt.Rows[this.ShipperPoints.Item1][j].ToString()))
                {
                    shipper = dt.Rows[this.ShipperPoints.Item1][j].ToString();
                }
            }

            string[] shipperArray = shipper.Split('\n');
            IDictionary<string, string> shipperDict = new Dictionary<string, string>();
            for (int i = 0; i < shipperArray.Length; i++)
            {
                string key = "";
                switch (i)
                {
                    case 0:
                        key = "Customer";
                        shipperDict.Add(key, shipperArray[i]);
                        break;
                    case 1:
                        key = "Address";
                        shipperDict.Add(key, shipperArray[i]);
                        break;
                    case 2:
                        char[] delimiters = { ',', ' ' };
                        string[] citystate = shipperArray[2].Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        for (int j = 0; j < citystate.Length; j++)
                        {
                            if (String.IsNullOrEmpty(citystate[j]))
                            {
                                continue;
                            }
                            switch (j)
                            {
                                case 0:
                                    key = "City";
                                    shipperDict.Add(key, citystate[j]);
                                    break;
                                case 1:
                                    key = "State";
                                    shipperDict.Add(key, citystate[j]);
                                    break;
                                case 2:
                                    key = "ZipCode";
                                    shipperDict.Add(key, citystate[j]);
                                    break;
                            }
                        }
                        break;
                    default:
                        shipperDict.Add(shipperArray[i].Split(':')[0], shipperArray[i].Split(':')[1]);
                        break;
                }
            }

            Customer customer = new Customer();
            Address shipperAddress = new Address();
            shipperAddress.Type = AddressType.Shipper.ToString();
            List<Address> addresses = new List<Address>();
            addresses.Add(shipperAddress);
            shipperAddress.SortOrder = 1;
            customer.Addresses = addresses;
            foreach (var item in shipperDict)
            {
                switch (item.Key)
                {
                    case "Customer":
                        customer.Name = item.Value;
                        break;
                    case "Cell":
                        customer.PhoneNo = item.Value;
                        break;
                    case "Address":
                        shipperAddress.Street1 = item.Value;
                        break;
                    case "State":
                        shipperAddress.State = item.Value;
                        break;
                    case "City":
                        shipperAddress.City = item.Value;
                        break;
                    case "ZipCode":
                        shipperAddress.ZipCode = item.Value;
                        break;
                }
            }

            invoice.Customer = customer;
        }

        private void parseOrigin(DataTable dt, Invoice invoice)
        {
            var origin = "";
            for (var j = 1; j < this.DestinationPoints.Item2; j++)
            {
                if (!String.IsNullOrEmpty(dt.Rows[this.ShipperPoints.Item1][j].ToString()))
                {
                    origin = dt.Rows[this.ShipperPoints.Item1][j].ToString();
                }
            }

            string[] originArray = origin.Split('\n');
            IDictionary<string, string> originDict = new Dictionary<string, string>();

            for (int i = 0; i < originArray.Length; i++)
            {
                string key = "";
                switch (i)
                {
                    case 0:
                        key = "Address";
                        originDict.Add(key, originArray[i]);
                        break;
                    case 1:
                        char[] delimiters = { ',', ' ' };
                        string[] citystate = originArray[i].Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        for (int j = 0; j < citystate.Length; j++)
                        {
                            if (String.IsNullOrEmpty(citystate[j]))
                            {
                                continue;
                            }
                            switch (j)
                            {
                                case 0:
                                    key = "City";
                                    originDict.Add(key, citystate[j]);
                                    break;
                                case 1:
                                    key = "State";
                                    originDict.Add(key, citystate[j]);
                                    break;
                                case 2:
                                    key = "ZipCode";
                                    originDict.Add(key, citystate[j]);
                                    break;
                            }
                        }
                        break;
                    default:
                        originDict.Add(originArray[i].Split(':')[0], originArray[i].Split(':')[1]);
                        break;
                }
            }

            Address originAddress = new Address();
            originAddress.Type = AddressType.Origin.ToString();
            originAddress.SortOrder = 2;
            invoice.Customer.Addresses.Add(originAddress);
            foreach (var item in originDict)
            {
                switch (item.Key)
                {
                    case "Address":
                        originAddress.Street1 = item.Value;
                        break;
                    case "State":
                        originAddress.State = item.Value;
                        break;
                    case "City":
                        originAddress.City = item.Value;
                        break;
                    case "ZipCode":
                        originAddress.ZipCode = item.Value;
                        break;
                    case "Floors/Flights":
                        originAddress.Floors = Convert.ToDouble(item.Value);
                        break;
                    case "Walking":
                        originAddress.Walking = Convert.ToInt32(item.Value);
                        break;
                    case "Type":
                        originAddress.PropertyType = item.Value;
                        break;
                }
            }
            invoice.Customer.Addresses.Add(originAddress);
        }

        private void parseDestination(DataTable dt, Invoice invoice)
        {
            var dest = "";
            for (var j = 1; j < colCount; j++)
            {
                if (!String.IsNullOrEmpty(dt.Rows[this.ShipperPoints.Item1][j].ToString()))
                {
                    dest = dt.Rows[this.ShipperPoints.Item1][j].ToString();
                }
            }

            var destArray = dest.Split('\n').ToList();
            if (!destArray.Contains("Public Storage"))
                destArray.Insert(0, ""); //Add a blank element in the array for possible additions to the address.

            IDictionary<string, string> destDict = new Dictionary<string, string>();
            foreach (var item in destArray)
            {
                string key = "";
                switch (destArray.IndexOf(item))
                {
                    case 0:
                        key = "Description";
                        destDict.Add(key, item);
                        break;
                    case 1:
                        key = "Address";
                        destDict.Add(key, item);
                        break;
                    case 2:
                        char[] delimiters = { ',', ' ' };
                        string[] citystate = item.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        for (int j = 0; j < citystate.Length; j++)
                        {
                            if (String.IsNullOrEmpty(citystate[j]))
                            {
                                continue;
                            }
                            switch (j)
                            {
                                case 0:
                                    key = "City";
                                    destDict.Add(key, citystate[j]);
                                    break;
                                case 1:
                                    key = "State";
                                    destDict.Add(key, citystate[j]);
                                    break;
                                case 2:
                                    key = "ZipCode";
                                    destDict.Add(key, citystate[j]);
                                    break;
                            }
                        }
                        break;
                    default:
                        destDict.Add(item.Split(':')[0], item.Split(':')[1]);
                        break;
                }
            }
            Address destAddress = new Address();
            destAddress.SortOrder = 3;
            destAddress.Type = AddressType.Destination.ToString();

            invoice.Customer.Addresses.Add(destAddress);
            foreach (var item in destDict)
            {
                switch (item.Key)
                {
                    case "Address":
                        destAddress.Street1 = item.Value;
                        break;
                    case "State":
                        destAddress.State = item.Value;
                        break;
                    case "City":
                        destAddress.City = item.Value;
                        break;
                    case "ZipCode":
                        destAddress.ZipCode = item.Value;
                        break;
                    case "Floors/Flights":
                        destAddress.Floors = Convert.ToDouble(item.Value);
                        break;
                    case "Walking":
                        destAddress.Walking = Convert.ToInt32(item.Value);
                        break;
                    case "Type":
                        destAddress.PropertyType = item.Value;
                        break;
                    case "Description":
                        destAddress.Description = item.Value;
                        break;
                }
            }
            invoice.Customer.Addresses.Add(destAddress);
        }

        private string[] parseVolume(DataTable dt)
        {

            for (int j = 0; j < colCount; j++)
            {
                if (!String.IsNullOrEmpty(dt.Rows[dt.Rows.Count][j].ToString()))
                {
                    return dt.Rows[dt.Rows.Count][j].ToString().Split('/');
                }
            }

            return Array.Empty<string>();
        }

        private Tuple<int, int> GetStartPoint(DataSet ds, string value, bool whole = true)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                {
                    if (whole)
                    {
                        if (ds.Tables[0].Rows[i][j].ToString() == value)
                        {
                            return new Tuple<int, int>(i, j);
                        }
                    }
                    else
                    {
                        if (ds.Tables[0].Rows[i][j].ToString().Contains(value))
                        {
                            return new Tuple<int, int>(i, j);
                        }
                    }
                }
            }

            return null;
        }


    }
}
