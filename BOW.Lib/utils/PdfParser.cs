﻿using System;
using System.IO;
using BOW.Lib.Data;
using BOW.Lib.Invoices;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Serilog;

namespace BOW.Lib.utils
{
    public class PdfParser : IParser
    {
        ILogger _logger;
        IInvoiceService _invoiceService;
        public PdfParser(ILogger logger, IInvoiceService invoiceService) 
        {
            _logger = logger;
            _invoiceService = invoiceService;
        }

        public bool Process(MemoryStream ms)
        {
            try
            {
                SplitPdfFile(ms);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return true;
        }

        private void SplitPdfFile(MemoryStream stream)
        {
            //// Intialize a new PdfReader instance with the contents of the source Pdf file:  
            PdfReader reader = new PdfReader(stream.ToArray());

            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
                _logger.Information("Processing Page {PageNo}", i);
                MemoryStream m = SplitPage(i, stream);
                Byte[] b = convertPdfToExcel(m);
                using (var ms = new MemoryStream(b))
                {
                    Invoice invoice = new Invoice();
                    var xlsParser = new XlsParser(invoice);
                    var invoiceProcess = xlsParser.Process(ms);
                    if (invoiceProcess)
                    {
                        _logger.Information("Adding invoice {InvoiceNo}", invoice.InvoiceNo);
                        _invoiceService.Save(invoice);
                        using (var ctx = new bowContext())
                        {
                            ctx.Invoices.Add(invoice);
                            try
                            {
                                ctx.SaveChanges();
                            }
                            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                            {
                                Exception raise = ex;
                                foreach (var validationErrors in ex.EntityValidationErrors)
                                {
                                    foreach (var validationError in validationErrors.ValidationErrors)
                                    {
                                        string message = string.Format("{0}:{1}",
                                            validationErrors.Entry.Entity.ToString(),
                                            validationError.ErrorMessage);
                                        // raise a new exception nesting
                                        // the current instance as InnerException
                                        raise = new InvalidOperationException(message, raise);
                                    }
                                }
                                throw raise;
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                }
            }
        }

        private MemoryStream SplitPage(int startPage, MemoryStream stream)
        {
            using (PdfReader reader = new PdfReader(stream.ToArray()))
            {
                MemoryStream newPdfStream = new MemoryStream();
                var pdfDoc = new Document();
                var pdf = new PdfCopy(pdfDoc, newPdfStream);
                pdfDoc.Open();
                pdf.AddPage(pdf.GetImportedPage(reader, startPage));
                pdfDoc.Close();

                Byte[] content = newPdfStream.ToArray();
                using (var ms = new MemoryStream(content))
                {
                    return ms;
                }
            }
        }

        private Byte[] convertPdfToExcel(MemoryStream m)
        {
            SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();
            f.XmlOptions.ConvertNonTabularDataToSpreadsheet = true;
            f.OpenPdf(m.ToArray());
            var result = f.ToExcel();
            f.ClosePdf();

            return result;
        }
    }
}
