﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.utils
{
    public interface IParser
    {
        bool Process(MemoryStream stream);
    }
}
