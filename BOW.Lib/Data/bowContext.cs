﻿using BOW.Lib.Customers;
using BOW.Lib.InventoryStuff;
using BOW.Lib.Invoices;
using BOW.Lib.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Data
{
    public partial class bowContext : DbContext
    {
        public bowContext() : base("name=bowContext") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new InvoiceEntityConfiguration());
            modelBuilder.Configurations.Add(new AddressEntityConfiguration());
            modelBuilder.Configurations.Add(new InventoryEntityConfiguration());
            modelBuilder.Configurations.Add(new MaterialsEntityConfiguration());
            modelBuilder.Configurations.Add(new ServiceEntityConfiguration());
        }

        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Address> Address { get; set; }

    }
}
