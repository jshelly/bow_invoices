﻿using BOW.Lib.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Data
{
    public class ServiceEntityConfiguration: EntityTypeConfiguration<Service>
    {
        public ServiceEntityConfiguration()
        {
            this.ToTable("Services");

            this.HasKey<Guid>(i => i.Id);

        }
    }
}
