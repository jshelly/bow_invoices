﻿using BOW.Lib.InventoryStuff;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Data
{
    public class MaterialsEntityConfiguration: EntityTypeConfiguration<Material>
    {
        public MaterialsEntityConfiguration()
        {
            this.ToTable("Materials");

            this.HasKey<Guid>(i => i.Id);

        }
    }
}
