﻿using BOW.Lib.Invoices;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Data
{
    public class InvoiceEntityConfiguration: EntityTypeConfiguration<Invoice>
    {
        public InvoiceEntityConfiguration()
        {
            this.ToTable("Invoices");

            this.HasKey<Guid>(i => i.Id);

            this.Property(i => i.InvoiceNo)
                .IsRequired();

            this.Property(i => i.EstimatedSrvDate)
                .IsRequired();

            this.Property(i => i.EstimatedArrTime)
                .IsRequired();


        }
    }
}
