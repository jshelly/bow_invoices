﻿using BOW.Lib.Customers;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Data
{
    public class AddressEntityConfiguration : EntityTypeConfiguration<Address>
    {
            public AddressEntityConfiguration()
            {
                this.ToTable("Addresses");

                this.HasKey<Guid>(i => i.Id);
            }
        }
}
