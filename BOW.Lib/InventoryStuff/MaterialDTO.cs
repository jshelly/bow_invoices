﻿using BOW.Lib.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.InventoryStuff
{
    public class MaterialDTO
    {
        public Guid Id { get; set; }
        public String Description { get; set; }
        public int Count { get; set; }
        public Guid InvoiceId { get; set; }
        public InvoiceDTO Invoice { get; set; }
    }
}
