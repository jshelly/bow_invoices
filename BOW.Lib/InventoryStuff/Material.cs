﻿using BOW.Lib.Invoices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.InventoryStuff
{
    public class Material
    {
        public Material()
        {
            this.Id = Guid.NewGuid();

        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set;  }
        public String Description { get; set; }
        public int Count { get; set; }
        public Guid InvoiceId { get; set; }
        public Invoice Invoice { get; set; }
    }
}
