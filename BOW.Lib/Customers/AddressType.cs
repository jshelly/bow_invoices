﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Customers
{
    public enum AddressType
    {
        Shipper,
        Origin,
        Destination,
        Stop1,
        Stop2
    }
}
