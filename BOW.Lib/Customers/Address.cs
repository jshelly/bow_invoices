﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Customers
{
    public class Address
    {
        public Address()
        {
            this.Id = Guid.NewGuid();
        }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Street1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Type { get; set; }        
        public string Description { get; set; }
        public double Floors { get; set; }
        public int Walking { get; set; }
        public int SortOrder { get; set; }
        public string PropertyType { get; set; }
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}
