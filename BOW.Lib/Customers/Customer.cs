﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Customers
{
    public class Customer
    {
        public Customer()
        {
            this.Id = Guid.NewGuid();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public List<Address> Addresses { get; set; }
    }
}
