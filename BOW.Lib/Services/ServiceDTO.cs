﻿using BOW.Lib.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Services
{
    public class ServiceDTO
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public double Rate { get; set; }
        public double Amount { get; set; }
        public Guid InvoiceId { get; set; }
        public InvoiceDTO Invoice { get; set; }
    }
}
