﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOW.Lib.Customers;
using BOW.Lib.InventoryStuff;
using BOW.Lib.Services;

namespace BOW.Lib.Invoices
{
    public class Invoice
    {
        public Invoice()
        {
            this.Id = Guid.NewGuid();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public int InvoiceNo { get; set; }
        public int QuoteNo { get; set; }
        public InvoiceType Type { get; set; }
        public bool NotToExceed { get; set; }
        public bool Binding { get; set; }
        public string EstimatedSrvDate { get; set; }
        public string EstimatedArrTime { get; set; }

        public List<Inventory> Inventories { get; set; }
        public List<Material> Materials { get; set; }
        public List<Service> Services { get; set; }
        public Customer Customer { get; set; }
    }
}
