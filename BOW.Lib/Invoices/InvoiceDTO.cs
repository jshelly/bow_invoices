﻿using BOW.Lib.Customers;
using BOW.Lib.InventoryStuff;
using BOW.Lib.Services;
using System;
using System.Collections.Generic;

namespace BOW.Lib.Invoices
{
    public class InvoiceDTO
    {
        public Guid Id { get; set; }
        public int InvoiceNo { get; set; }
        public int QuoteNo { get; set; }
        public InvoiceType Type { get; set; }
        public bool NotToExceed { get; set; }
        public bool Binding { get; set; }
        public string EstimatedSrvDate { get; set; }
        public string EstimatedArrTime { get; set; }

        public IEnumerable<InventoryDTO> Inventories { get; set; }
        public IEnumerable<MaterialDTO> Materials { get; set; }
        public IEnumerable<ServiceDTO> Services { get; set; }
        public CustomerDTO Customer { get; set; }
    }
}
