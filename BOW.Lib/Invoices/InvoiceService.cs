﻿using BOW.Lib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Invoices
{
    public class InvoiceService : IInvoiceService
    {
        private readonly bowContext _context;
        public InvoiceService(bowContext context)
        {
            _context = context;
        }
        public bool Delete(Guid id)
        {
            Invoice invoice = _context.Invoices.First<Invoice>(x => x.Id == id);
            _context.Invoices.Attach(invoice);
            _context.Invoices.Remove(invoice);
            var result = _context.SaveChanges();
            return result > 0;
        }

        public IEnumerable<Invoice> Invoices()
        {
            return _context.Invoices
                    .Include("Customer")
                    .Include("Customer.Addresses")
                    .Include("Inventories")
                    .Include("Materials")
                    .Include("Services");
        }

        public Invoice Save(Invoice invoice)
        {
            _context.Invoices.Add(invoice);
            _context.SaveChanges();

            return invoice;
        }
    }
}
