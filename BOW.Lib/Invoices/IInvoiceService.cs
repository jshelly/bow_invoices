﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOW.Lib.Invoices
{
    public interface IInvoiceService
    {
        IEnumerable<Invoice> Invoices();
       
        Invoice Save(Invoice invoice);

        bool Delete(Guid id);
        
    }
}
